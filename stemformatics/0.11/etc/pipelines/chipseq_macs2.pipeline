index="%GENOME_INDEX%"
gtf="%GTF_FILE%"
genome="%GENOME%"
genomesize="%GENOME_SIZE%"
peaktype="%PEAK_TYPE%"
macsformat="%MACS_INPUT_FORMAT%"
macsqvalue="%MACS_QVALUE%"
mismatches="%MISMATCHES%"
## N CPUs = max N library peak calls in parallel
NCPU=%NCPU%
R_BIN="%R_BIN%"
MACS2_BIN="%MACS2_BIN%"

dsdir="%DATASET_DIR%"
fastqdir="$dsdir/source/raw"
outdir="$dsdir/source/%PROCESSED_OUTDIR%"
qcout="$outdir/fastqc"
bamout="$outdir/aligned"
peaksout="$outdir/peakcalls"
chipqcout="$outdir/chipqc"
annotatedpeaks="$outdir/annotatedpeaks"

mkdir -p "$qcout" "$bamout"
targets="$fastqdir/targets.txt"

### =========================================================================

## FASTQC report (multi-threaded, update-only mode)
s4m subread::fastqc -i "$fastqdir" -o "$qcout" -t "$targets" -u -N $NCPU

## subread alignment (update-only mode)
s4m subread::fastq2bam -i "$fastqdir" -o "$bamout" -t "$targets" -x "$index" --mismatches $mismatches --indels 0 -u -N $NCPU -R $R_BIN

## sort and index output BAMs (required for ChIPQC)
s4m samtools::sortbams -i "$bamout" -N $NCPU

## read alignment stats and report
s4m samtools::mapstats -i "$bamout" -o "$bamout"

## MACS peak calling
s4m MACS2::callpeaks -t "$targets" -i "$bamout" -o "$peaksout" -g "$genomesize" -p "$peaktype" -f "$macsformat" -q "$macsqvalue" -N $NCPU -x "$MACS2_BIN"

## ChIPQC
fbase=`basename "$targets" .txt`
s4m chipqc::qcreport -t "$peaksout/${fbase}_chipqc.txt" -o "$chipqcout" -R $R_BIN

## Finds *.narrowPeak or *.broadPeak
peakfiles=`ls -1 "$peaksout"/*Peak | tr "\\n" ","`
s4m HOMER::annotatepeaks -i "$peaksout" -f "$peakfiles" -o "$annotatedpeaks" -g "$genome" -G "$gtf"
