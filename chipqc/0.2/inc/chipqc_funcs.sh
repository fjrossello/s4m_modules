#!/bin/sh

## Check that the sample table satisfies requirements for ChIPQC to work
##
## Unit Tests:
##   test_chipqc.sh:testInputSamplesFileValidation()
##   test_chipqc.sh:testInputSamplesFileValidationBad()
##
chipqc_validate_sampletable () {
  cvs_sampletable="$1"
  if [ ! -f "$cvs_sampletable" ]; then
    s4m_error "ChIPQC sample table '$cvs_sampletable' not found!"
    return 1
  fi

  ## Check file format:

  ## Check mandatory headers
  cvs_mandatory_headers="SampleID
Factor
Replicate
bamReads
Peaks"

  ret=0
  for header in $cvs_mandatory_headers
  do
    head -1 "$cvs_sampletable" | grep "\b$header\b" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      ret=1
      s4m_error "ChIPQC input sample table missing mandatory column '$header'"
    fi
  done
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Either none or both of 'ControlID' and 'bamControl' must be specified
  ## (if a control is used by a ChIP library, the path to the control BAM
  ## must also be given)
  head -1 "$cvs_sampletable" | grep "\bControlID\b" > /dev/null 2>&1
  hasControlID=$?
  head -1 "$cvs_sampletable" | grep "\bbamControl\b" > /dev/null 2>&1
  hasBamControl=$?
  if [ $hasControlID -eq 0 ]; then
    if [ $hasBamControl -ne 0 ]; then
      s4m_error "ChIPQC input sample table must use both 'ControlID' and 'bamControl' fields (if at all)"
      return 1
    fi
  elif [ $hasBamControl -eq 0 ]; then
    if [ $hasControlID -ne 0 ]; then
      s4m_error "ChIPQC input sample table must use both 'ControlID' and 'bamControl' fields (if at all)"
      return 1
    fi
  fi

  ## Not checking content of file at this stage - relying on ChIPQC to fail elegantly if it were to do so
}

