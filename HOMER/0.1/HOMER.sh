#/bin/sh
# Module: HOMER
# Author: Othmar Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## Include functions
. ./inc/HOMER_funcs.sh



## Annotate peak files
##
annotatepeaks_handler () {
  ah_peakfiles="$HOMER_peakfiles"
  ah_peakfiles_list=`annotatepeaks_parse_list "$ah_peakfiles"`
  ah_genome="$HOMER_genome"
  ah_gtffile="$HOMER_gtf"
  ah_inputdir="$HOMER_inputdir"
  ah_outdir="$HOMER_outputdir"

  if [ ! -d "$ah_outdir" ]; then
    mkdir -p "$ah_outdir"
    if [ $? -ne 0 ]; then
      s4m_error "Failed to create output path '$ah_outdir'"
      return 1
    fi
  fi

  ### DEBUG ###
  for pf in $ah_peakfiles_list
  do
    s4m_debug "Got peak file: [$pf]"
  done
  ### END ###

  ## iterate over each peak file and run annotatePeaks.pl
  for pf in $ah_peakfiles_list
  do
    fbase=`basename "$pf"`
    if [ ! -f "$ah_inputdir/$fbase" ]; then
      s4m_error "Input peak file '$ah_inputdir/$fbase' not found!"
      return 1
    fi
    ## HOMER outputs some temp files, so make sure we're in the output directory
    cd "$ah_outdir"
    annotatePeaks.pl "$ah_inputdir/$fbase" "$ah_genome" -gtf "$ah_gtffile" -annStats "$ah_outdir/${fbase}.${ah_genome}.stats.txt" > "$ah_outdir/${fbase}.${ah_genome}.annotated.txt"
    ret=$?
    cd $OLDPWD
    if [ $ret -ne 0 ]; then
      return $ret
    fi
  done

}


main () {

  s4m_route annotatepeaks annotatepeaks_handler

}

### START ###
main

