#!/bin/sh

## If no user NCPUs given, use this many threads in multi-process steps.
SUBREAD_DEFAULT_NTHREADS=2; export SUBREAD_DEFAULT_NTHREADS

## Default max #mismatches allowed in alignment
SUBREAD_DEFAULT_MISMATCHES=5; export SUBREAD_DEFAULT_MISMATCHES

## Poll wait time for batch FASTX quality stats completion checking
SUBREAD_FASTX_WAIT_SECONDS=60; export SUBREAD_WAIT_SECONDS

##
## Test whether we're in dryrun mode
##
is_subread_dryrun () {
  if [ "$SUBREAD_DRYRUN" = "TRUE" ]; then
    echo "true"
    return
  fi
  echo "false"
  return 1
}


##
## Get FASTQ file names from a directory
##
subread_get_fastq_files () {
  sgff_fastqdir="$1"
  ls -1 "$sgff_fastqdir/" | grep -P "\.(fastq|fq)$"
}


##
## Get fastq file names from targets file (strips leading paths)
## 
## NOTE: Assumes targets file is valid
##
subread_get_fastq_names_from_targets () {
  sgft_targets="$1"

  if [ -f "$sgft_targets" ]; then

    retfiles=""
    rfcols=`head -1 "$sgft_targets" | tr "\t" "\n" | grep -n -P "ReadFile[12]" | cut -d':' -f 1`

    for c in $rfcols
    do
      files=`cut -f $c "$sgft_targets" | grep -v ReadFile | grep -P "\w"`
      for f in $files
      do
        retfiles="$retfiles
`basename $f`"
      done
    done

    echo "$retfiles" | grep -P "\w" | sed -r 's| +||g'
    return
  fi
  return 1
}


##
## Get BAM file (base) name for a FASTQ file defined in targets file.
## 
## FASTQ name matches in ReadFile1 or ReadFile2 and returns Sample ID
##
## NOTE: Assumes targets file is valid
##
subread_get_bamname_for_fastq () {
  sgbff_fastq="$1"
  sgbff_targets="$2"

  if [ -f "$sgbff_targets" ]; then
    targetline=`grep -P "\b$sgbff_fastq" "$sgbff_targets" 2> /dev/null`
    if [ $? -eq 0 ]; then
      echo "$targetline" | cut -f 1 | tr -d [:cntrl:]
      return
    fi
  fi
  return 1
}


##
## Check that a targets file looks valid
##
validate_targets_file () {
  vtf_targets="$1"

  ## Exists, non-zero
  if [ ! -s "$vtf_targets" ]; then
    return 1
  fi

  ## Minimum header
  grep -P "^SampleID\t.*SampleType.*\tReadFile1" "$vtf_targets" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    return 1
  fi

  ## At least one sample specified
  nlines=`cat "$vtf_targets" | wc -l`
  if [ $nlines -lt 2 ]; then
    return 1
  fi

  ## Has at least one valid FASTQ path
  cutcol=`head -1 "$vtf_targets" | tr "\t" "\n" | grep -n ReadFile1 | cut -d':' -f 1`
  fqpaths=`cut -f "$cutcol" "$vtf_targets" | grep -P "\w" | wc -l`
  if [ $fqpaths -lt 1 ]; then
    return 1
  fi
}


##
## Get the base file name of a FASTQ file
##
subread_fastq_basename () {
  sfb_fqfile="$1"
  echo "$sfb_fqfile" | grep -P "\.fastq$" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    fbase=`basename "$sfb_fqfile" .fastq`
  else
    fbase=`basename "$sfb_fqfile" .fq`
  fi
  echo "$fbase"
}


##
## Look for base or colour space index data file.
## Returns match if found, or blank string if not found.
##
subread_find_index_file () {
  sfif_indexdir="$1"
  sfif_indexpattern="00.[bc].tab\$"
  matches=`ls "$sfif_indexdir" | grep -P "$sfif_indexpattern" 2> /dev/null`
  if [ $? -eq 0 ]; then
    nmatches=`echo "$matches" | wc -l 2> /dev/null`
    if [ $nmatches -eq 1 ]; then
      echo "$matches" | tr -d [:cntrl:]
      return
    fi
  fi
  echo ""
}
