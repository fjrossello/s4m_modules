#/bin/sh
# Module: multiqc
# Author: O.Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

MULTIQC_MINICONDA_HOME="./lib/miniconda2"
MULTIQC_ENV="multiqc_env"


## TODO: Move into inc/multiqc_funcs.sh
check_multiqc_env () {
  if [ ! -d $MULTIQC_MINICONDA_HOME/envs/$MULTIQC_ENV ]; then
    s4m_error "Module multiqc needs to complete one-time setup ( '$S4M_THIS_MODULE/setup.sh' )"
    return 1
  fi
  return 0
}


## TODO: Move into inc/multiqc_funcs.sh
do_multiqc () {
  dm_inputdir="$1"
  dm_outdir="$2"

  if check_multiqc_env; then
    . $MULTIQC_MINICONDA_HOME/bin/activate $MULTIQC_ENV
    multiqc --version
    ## NOTE: Always forcibly overwrite existing outputs
    multiqc --force -o "$dm_outdir" "$dm_inputdir"
    return $?

  else
    return 1
  fi
}


## Generate MultiQC report
multiqc_handler () {
  do_multiqc "$multiqc_inputdir" "$multiqc_outdir"
}


## Return version of MultiQC installed
multiqc_version_handler () {

  if check_multiqc_env; then
    . $MULTIQC_MINICONDA_HOME/bin/activate $MULTIQC_ENV
    multiqc --version
  else
    return $?
  fi
}



main () {

  s4m_route multiqc multiqc_handler
  s4m_route version multiqc_version_handler

}

### START ###
main

