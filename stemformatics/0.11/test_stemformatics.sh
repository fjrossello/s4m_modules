#!/bin/sh
## test_stemformatics.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/stemformatics_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests (uncomment if needed)
#setUp () {
#}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals 0 $?
}

## TODO: More tests

