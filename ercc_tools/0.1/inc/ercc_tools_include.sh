#!/bin/sh

##
## Plot ERCC input molecules vs output read counts
##
ercc_do_recovery_qc () {
  edaq_readcounts="$1"
  edaq_molecules="$2"
  edaq_outdir="$3"

  if s4m_isdebug; then
    edaq_extraflags="-debug=TRUE"
  fi
  call_R "$ercc_tools_R" "$S4M_THIS_MODULE/scripts/ercc_reads_vs_molecules.R" "-readcounts='$edaq_readcounts' -molecules='$edaq_molecules' -outdir='$edaq_outdir' $edaq_extraflags"
  return $?
}

##
## Plot mapping stats boxplot (for large samples N)
##
ercc_do_mapstats_qc () {
  edmq_mapstatsfile="$1"
  edmq_outdir="$2"

  if s4m_isdebug; then
    edmq_extraflags="-debug=TRUE"
  fi
  call_R "$ercc_tools_R" "$S4M_THIS_MODULE/scripts/plot_mapstats_qc_boxplot.R" "-mapstatsfile='$edmq_mapstatsfile' -outdir='$edmq_outdir' $edmq_extraflags"
}

