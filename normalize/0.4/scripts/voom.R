## ==========================================================================
## File:	voom.R	
## Author:	Othmar Korn <o.korn@uq.edu.au>
## Modified:	2016-10-10
## Description:	Perform normalisation with Voom mean-variance trend output.
##		NOTE: Returns scaled (TMM by default) CPM or RPKM log counts
## ==========================================================================

## Include util scripts (relative to module root path)
source("scripts/inc/util.R")
## TODO: Remove if not needing cpm2rpkm() function
#sourcE("scripts/inc/sequtil.R")
library(limma)
## edgeR: For cpm() only
library(edgeR)


## Print warnings as they occur
options(warn=1)
## Expression table or GCT file
inputfile <- ARGV.get("input",required=T)
## Output expression table file
outputfile <- ARGV.get("output",required=T)
## As per: http://bioinf.wehi.edu.au/RNAseqCaseStudy/
## Only required if "plotmeanvariance" is TRUE (usually the case)
targetsfile <- ARGV.get("targets")
## Any pre-normalisation (scaling) method supported by calcNormFactors()
normmethod <- ifelse(is.na(ARGV.get("normmethod")), "TMM", as.character(ARGV.get("normmethod")))
## Toggle plotting of voom mean-variance trend, takes boolean argument (default TRUE)
plotmeanvariance <- ifelse(is.na(ARGV.get("plotmeanvariance")), TRUE, as.logical(ARGV.get("plotmeanvariance")))
## Post-norm scaling, if any. Accepts any type supported by normalizeBetweenArrays().
## Default is "scale" (median center)
postnorm <- ifelse(is.na(ARGV.get("postnorm")), "scale", as.character(ARGV.get("postnorm")))
## Optional override phenotype column from targets with column name or index
pheno_column <- ARGV.get("pheno_column")
## Output count format - CPM (default) or RPKM
## If "RPKM", genelengths flag must be given (see below)
countformat <- ifelse(is.na(ARGV.get("countformat")), "CPM", as.character(ARGV.get("countformat")))

genelengths <- NA
genelengthsTable <- NA
if (countformat == "RPKM") {
  ## A gene lengths table (gene ID first column, gene size from "Length" column, or second if only two columns in total)
  ## e.g. the annotation table output from R/Subread featureCounts() can be used here directly (has "Length" as 6th column).
  ## NOTE: Gene lengths gene ID order MUST match that exactly of gene IDs in input data
  genelengths <- ARGV.get("genelengths",required=T)
  if (! file.exists(genelengths)) {
    stop(paste("Error: Input gene lengths table '",genelengths,"' not found!",sep=""))
  }
  ## Assume gene IDs in column index 1 and lengths in column with header "Length"
  ## (R/Subread featureCounts 7-column count annotation format)
  lencol <- "Length"
  genecol <- 1
  genelengthsTable <- read.table(genelengths, sep="\t", header=T, row.names=1, quote="", as.is=T, check.names=F, stringsAsFactors=F)
  if (! lencol %in% colnames(genelengthsTable)) {
    ## Two column where gene IDs are row.names
    if (ncol(genelengthsTable) == 1) {
      lencol <- 1
      ## if zero further on, we use row.names()
      genecol <- 0
    } else {
      stop("Error: Failed to determine gene lengths column in input gene lengths table!")
    }
  }
}

## Prepare model matrix if peforming Voom mean-variance trend
if (plotmeanvariance == TRUE) {
  if (is.na(targetsfile)) {
    stop("Error: Voom mean-variance trend analysis requires targets file input!")
  }
  ## Default phenotype target SampleType or CellType (second column) unless overriden
  targetcol <- 2
  if (! is.na(pheno_column)) {
    targetcol <- as.character(pheno_column)
  }
  ## Load targets file
  targets <- readTargets(as.character(targetsfile))
  phenotype <- targets[,targetcol]
  ## If pairwise contrasts given, use them and proceed to ignore other values
  ## from phenotype file that are not used
  designLevels <- unique(phenotype)
  if (length(designLevels) < 2) {
    stop("Error: Experimental design requires more than one level in phenotype factor!")
  }

  ## Simple design testing differences across several groups, single factor
  ## See Limma User's Guide for examples for many experimental models.
  ## Voom needs this to calculate linear regression weights.
  design <- model.matrix(~0 + factor(phenotype, levels=designLevels))
  colnames(design) <- make.names(designLevels)

  ### DEBUG ###
  print("DEBUG: Got design:")
  print(design)
  print("DEBUG: Got designLevels: ")
  print(designLevels)
  Sys.sleep(3)
  ### END DEBUG ###
}

inputfile <- as.character(inputfile)
outputfile <- as.character(outputfile)
outdir <- dirname(outputfile)

## Load input count table
dat <- read.table(inputfile, sep="\t", header=T, check.names=F, as.is=T, row.names=1)

## If loaded a gene lengths table, ensure that gene IDs match in quality and quantity
if (countformat == "RPKM") {
  if (genecol == 0) {
    geneids <- row.names(genelengthsTable)
  } else {
    geneids <- genelengthsTable[,genecol]
  }
  if (! identical(row.names(dat), geneids)) {
    stop("Error: Gene IDs in gene lengths table does not match that of input count table!")
  }
}

## Get the base name of the input file to craft output file names based on it
fbase <- basename(inputfile)
fbase <- gsub(".txt", "", fbase)

## Plot Voom mean-variance trend
if (plotmeanvariance == TRUE) {
  print("Plotting Voom mean-variance trend..")

  datDGE <- DGEList(counts=dat)
  ## Apply pre-Voom normalisation
  datDGE <- calcNormFactors(datDGE, method=normmethod)
  bitmap(paste(outdir,"/",fbase,"_voom_mean_variance.png",sep=""),type="png16m",width=1000, height=1000, units="px")
  ## Voom with median centering on internal voom transformed log2 CPM counts
  ## "voomcenter" is now an EList/DGEList object with log CPM counts in '$E' attribute
  voomDGE <- voom(datDGE, design, plot=TRUE)
  dev.off()
} else {
  print("Skipping plotting of Voom mean-variance trend..")
}


## NOTE: Not used but including here for reference - how one would extract Voom's CPM and RPKM counts
#if (countformat == "RPKM") {
#  genelen <- genelengthsTable[,lencol]
#  print(paste("Writing RPKM log2 counts.."))
#  voomRPKMnorm <- rpkm(voomDGE, gene.length=genelen, log=T, normalized.lib.sizes=T)
#  write.table(voomRPKMnorm, outputfile, sep="\t", quote=F)
#  
#} else {
#  print(paste("Writing CPM log2 counts.."))
#  voomCPMnorm = cpm(voomDGE, log=T, normalized.lib.sizes=T)
#  write.table(voomCPMnorm, outputfile, sep="\t", quote=F)
#}

## Perform manual normalisation - we don't want to return Voom's count table because they've
## added 0.5 reads to each count so we end up with all counts > 0 which doesn't reflect
## observations (e.g. zero read counts should remain zero).
##
## It doesn't matter for Voom since it's downstream is always going to be a limma DE analysis,
## but since we're only interested in the normalised counts and using Voom only to plot
## the mean-variance trend, we can perform our own scale normalisation without modifying the
## counts.

print(paste("Applying '",normmethod,"' normalisation..",sep=""))
dat <- as.matrix(dat)
f <- calcNormFactors(dat, method=normmethod)

## Output normalisation factors in case we want to know what they were
fbase <- basename(outputfile)
fbase <- gsub(".txt", "", fbase)
normfactors <- f
names(normfactors) <- colnames(dat)
print("Writing per-library normalisation factors..")
write.table(as.data.frame(normfactors), paste(outdir,"/",fbase,"_",normmethod,"_normFactors.txt",sep=""), sep="\t", quote=F, na="")


if (countformat == "RPKM") {
  genelen <- genelengthsTable[,lencol]
  ## NOTE: We don't ask for logged counts otherwise the prior.count of 0.25
  ## would be added to each raw count to avoid taking the log of zero.
  ## Instead, we post-hoc set any log2(0) to NA.
  scaledRPKM <- rpkm(dat, gene.length=genelen, log=F, lib.size=colSums(dat) * f)
  ## Remove zeroes to prevent possible zero (or less) log median issues arising from
  ## "scale" (median scaling) normalisation in normalizeBetweenArrays()
  scaledRPKM[ scaledRPKM == 0 ] <- NA
  if (postnorm != "none") {
    print(paste("Applying additional between sample ",postnorm," normalisation..",sep=""))
  }
  scaledRPKMLog2Norm <- log2(normalizeBetweenArrays(scaledRPKM, method=postnorm))
  scaledRPKMLog2Norm[ scaledRPKMLog2Norm == -Inf ] <- NA
  if (normmethod != "none") {
    print(paste("Writing '", normmethod, "' RPKM log2 counts..",sep=""))
  } else {
    print("Writing RPKM log2 counts..")
  }
  write.table(scaledRPKMLog2Norm, outputfile, sep="\t", quote=F, na="")

} else {
  ## NOTE: We don't ask for logged counts otherwise the prior.count of 0.25
  ## would be added to each raw count to avoid taking the log of zero.
  ## Instead, we post-hoc set any log2(0) to NA.
  scaledCPM <- cpm(dat, log=F, lib.size=colSums(dat) * f)
  ## Remove zeroes to prevent possible zero (or less) log median issues arising from
  ## "scale" (median scaling) normalisation in normalizeBetweenArrays()
  scaledCPM[ scaledCPM == 0 ] <- NA
  if (postnorm != "none") {
    print(paste("Applying additional between sample ",postnorm," normalisation..",sep=""))
  }
  scaledCPMLog2Norm <- log2(normalizeBetweenArrays(scaledCPM, method=postnorm))
  scaledCPMLog2Norm[ scaledCPMLog2Norm == -Inf ] <- NA
  if (normmethod != "none") {
    print(paste("Writing '", normmethod, "' CPM log2 counts..",sep=""))
  } else {
    print("Writing CPM log2 counts..")
  }
  write.table(scaledCPMLog2Norm, outputfile, sep="\t", quote=F, na="")
}


## Write sessionInfo() for R and library version info
writeLines(capture.output(sessionInfo()), paste(outdir,"/sessionInfo_voom.txt",sep=""))
