## Temporary buffer for target commands to pipeline
S4M_PIPELINE_CMDS=""; export S4M_PIPELINE_CMDS


## Execute line by line, test for non-zero status and break if error
## Report on how many steps were completed vs. total.
##
## NOTE: Input lines ending in "\" are automatically parsed by "while read line"
##       (SHELL behaviour) - so our script sees this as a single line *already*.
## NOTE: Capture of STDOUT broken for nested s4m output - disabling output redirection for now.
## NOTE: Line number corresponds to post-range and exclude filter application (if any)
run_commands () {
  #stdout="$1"
  #stderr="$2"

  linecount=0
  ## Read lines, ignore comment lines
  echo "$S4M_PIPELINE_CMDS" |
  while read line
  do
    linecount=`expr $linecount + 1`
    ## skip comment line or blank line
    echo "$line" | grep -P "^ *#" > /dev/null 2>&1
    if [ $? -eq 0 -o -z "$line" ]; then
      continue
    fi

    #cmd=`echo "$line" | sed 's|^ +||' | cut -d' ' -f 1`
    ## Is command an S4M command?
    ## Currently don't need (hopefully) to treat different to a non-S4M command,
    ## but if we ever do, can uncomment the below:

    #echo "$cmd" | grep -P "^ *$S4M_SCRIPT_NAME" > /dev/null 2>&1
    #if [ $? -eq 0 ]; then
    ## other shell command
    #else
    #fi

    ## Run command with STDOUT or STDERR redirects if necessary
    #if [ ! -z "$stdout" -a ! -z "$stderr" ]; then
    #  eval "$line"  > "$stdout" 2> "$stderr"
    #elif [ ! -z "$stdout" ]; then
    #  eval "$line"  > "$stdout"
    #elif [ ! -z "$stderr" ]; then
    #  eval "$line"  2> "$stderr"
    #else
    eval "$line"
    #fi

    ## If an error occurred, exit and report line number of command which failed
    if [ $? -ne 0 ]; then
      s4m_error "Pipeline command on line $linecount (post-filtering) exited with non-zero status:

  cmd>  $line
"
      return 1
    fi
  done
}


## Write post-parsed pipeline command file
save_command_file () {
  outfilepath="$1"

  if [ -z "$S4M_PIPELINE_CMDS" ]; then
    s4m_error "Can't save post-parsed pipeline file, no pipeline commands loaded!"
    return 1
  fi
  if [ -z "$outfilepath" ]; then
    s4m_error "No file path given to write post-parsed pipeline file!"
    return 1
  fi
  if [ -d "$outfilepath" ]; then
    s4m_error "The given post-parsed output file path must be a valid file path!"
    return 1
  fi
  if [ ! -d `dirname "$outfilepath"` ]; then
    s4m_error "The directory path for post-parsed output file does not exist!"
    return 1
  fi

  echo "$S4M_PIPELINE_CMDS" > "$outfilepath"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to write post-parsed pipeline file!"
    return 1
  else
    s4m_log "Saved post-parsed pipeline file to [$outfilepath]"
  fi
}


## Parse command lines from STDIN.
## Saves as a temp file and runs default "from file" load routine.
##
## NOTE: Needs testing!
##
load_command_file_stdin () {
  start="$1"
  end="$2"
  exclude_regex="$3"
  keepheader="$4"

  lcfs_ts=`date +"$S4M_TS_FORMAT"`
  ## dump STDIN to temp file
  pipeline_tmp="$S4M_TMP/pipeline_stdin.$$.$lcfs_ts"
  cat > "$pipeline_tmp"

  load_command_file_default "$pipeline_tmp" "$start" "$end" "$exclude_regex" "$keepheader"
  return $?
}


## Print header to STDOUT
## Returns: Numerical index of last header line
##
extract_command_file_header () {
  cmdfile="$1"
  first_module_cmd_index=`cat "$cmdfile" | grep -n -P "^$S4M_SCRIPT_NAME" | cut -d':' -f 1 | head -1  2> /dev/null`
  if [ $? -ne 0 -o -z "$first_module_cmd_index" ]; then
    s4m_warn "No module commands in target pipeline and 'keep header' flag given, retaining ALL input lines"
    cat "$cmdfile"
    last_cmdfile_line_index=`cat "$cmdfile" | wc -l`
    return $last_cmdfile_line_index
  else
    max_header_index=`expr $first_module_cmd_index - 1`
    sed -n "1,$max_header_index p" "$cmdfile"
    return $max_header_index
  fi
}


## Parse a command file from file and return contents, or an error status.
## Assumes "start" is less than or equal to "end" (if both supplied)
##
load_command_file_default () {
  cmdfile="$1"
  start="$2"
  end="$3"
  exclude_regex="$4"
  keepheader="$5"

  if [ -z "$start" ]; then
    start=1
  fi
  ## We'll be passing this to 'sed' so set to EOF now.
  if [ -z "$end" -o "$end" = "-" ]; then
    end='$'
  fi

  ## The subset of command file we'll keep after all filtering
  cmdfile_keep_lines=""

  if [ "$keepheader" = "true" ]; then
    cmdfile_keep_lines=`extract_command_file_header "$cmdfile"`
    last_header_line=$?
    cmdfile_keep_lines="$cmdfile_keep_lines
"

    ## If keeping the header then our start line is at least
    ## the very next non-header line.
    if [ $start -le $last_header_line ]; then
      #s4m_debug "load_command_file_default(): start $start <= $last_header_line : setting to $last_header_line + 1"
      start=`expr $last_header_line + 1`
    fi
  fi

  ## Append command file body, and regex filter body if needed
  cmdfile_body=`sed -n "$start,$end p" "$cmdfile"`
  if [ -z "$exclude_regex" ]; then
    cmdfile_keep_lines="${cmdfile_keep_lines}${cmdfile_body}"
  else
    s4m_debug "Applying regex filter.."
    cmdfile_body_filtered=`echo "$cmdfile_body" | grep -v -P "$exclude_regex"`
    cmdfile_keep_lines="${cmdfile_keep_lines}${cmdfile_body_filtered}"
  fi

  ## T#2513:
  ## Remove superfluous spaces between newline disabling slashes for
  ## natural multi-line input (if we need to print out the parsed line
  ## it looks much better without the extra spaces a user might have added)

  cmdfile_keep_lines=`echo "$cmdfile_keep_lines" | sed -r -e 's| +\\\\$| \\\|g'`

  ## T#2513:
  ## Remove any indented comments (they belong to a multi-line block) -
  ## otherwise any following commands would be treated as belonging to the
  ## comment line (causing early exit or non-execution of command lines)

  cmdfile_keep_lines=`echo "$cmdfile_keep_lines" | sed -r -e '/^ +\#+.*$/d'`
  
  if [ -z "$cmdfile_keep_lines" ]; then
    s4m_error "Post-parsed command file '$cmdfile' is empty, nothing to do!"
    return 1
  fi

  /bin/echo -n "$cmdfile_keep_lines"
}


## Check filtering options are sane (pre-validation)
## Some things can only be checked while attempting to parse the command file.
## e.g. see 'load_command_file_stdin()' and 'load_command_file_default()'
##
validate_filter_options () {
  start="$1"
  end="$2"
  exclude_regex="$3"

  echo "$start" | grep -P "^[0-9]+$" > /dev/null 2>&1
  if [ $? -ne 0 -o "$start" = "0" ]; then
    s4m_error "If supplied, pipeline starting line position must be a number >= 1"
    return 1
  fi

  if [ ! -z "$end" ]; then
    echo "$end" | grep -P "^[0-9]+$" > /dev/null 2>&1
    if [ $? -ne 0 -o "$end" = "0" ]; then
      s4m_error "If supplied, pipeline end line position must be a number >= 1 (default EOF)"
      return 1
    fi
    s4m_log "Got pipeline range filter lines $start:$end"
  fi

  if [ ! -z "$exclude_regex" ]; then
    s4m_log "Will filter out commands matching exclude pattern /$exclude_regex/ .."
  fi
}


## Load command file from file or STDIN.
##
## NOTE: Can't use "-" as STDIN file proxy due to the way S4M does its
##   argument processing, S4M core script would need an update.
##
load_command_file () {
  cmdfile="$1"
  start="$2"
  end="$3"
  exclude_regex="$4"
  keep_header="$5"

  s4m_log "Loading command file [$cmdfile].."

  if [ "$end" = "-" ]; then
    end=""
  fi
  validate_filter_options "$start" "$end" "$exclude_regex"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  cmdlines=""

  ## Process STDIN
  if [ $cmdfile = "STDIN" ]; then
    cmdlines=`load_command_file_stdin "$cmdfile" "$start" "$end" "$exclude_regex" "$keep_header"`
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi

  ## Not STDIN, and given name is not a file
  elif [ ! -f "$cmdfile" ]; then
    s4m_error "Command file '$cmdfile' not found! Check input path to pipeline file."
    return 1

  ## .. it's a file, load it
  else
    cmdlines=`load_command_file_default "$cmdfile" "$start" "$end" "$exclude_regex" "$keep_header"`
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi
  fi

  S4M_PIPELINE_CMDS="$cmdlines"; export S4M_PIPELINE_CMDS

  s4m_debug "Got command file contents = [$S4M_PIPELINE_CMDS]
"

}

