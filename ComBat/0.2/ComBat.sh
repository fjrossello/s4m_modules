#/bin/sh
# Module: ComBat
# Author: O.Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## ComBat includes
. ./inc/ComBat_funcs.sh


## Limma removeBatchEffect() handler
##
removebatch_handler () {
  do_removebatch "$ComBat_input" "$ComBat_phenofile" "$ComBat_outdir"
  return $?
}


## ComBat handler
##
combat_handler () {
  do_combat "$ComBat_input" "$ComBat_phenofile" "$ComBat_outdir"
  return $?
}


main () {
  ## Include R
  s4m_import Rutil/Rutil.sh

  s4m_route combat combat_handler
  s4m_route removebatch removebatch_handler
}

### START ###
main

