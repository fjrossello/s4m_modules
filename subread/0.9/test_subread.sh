#!/bin/sh
## test_subread.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  SUBREAD_TEST_FQDIR="./data"
  export SUBREAD_TEST_FQDIR
 
  ## Load functions being tested
  . ./inc/subread_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  rm -rf "$S4M_TMP/$$/"*.tab
  touch "$S4M_TMP/$$/foo.00.c.tab"
}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals 0 $?
}

testDryRunModeByBoolStringOrReturnStatus () {
  SUBREAD_DRYRUN="TRUE"; export SUBREAD_DRYRUN
  assertEquals "true" `is_subread_dryrun`
  is_subread_dryrun > /dev/null 2>&1
  assertEquals 0 $?
}

## Test FASTQ files found by extension
testFindFASTQFileRegex () {
  fqfiles=`ls -1 ./$SUBREAD_TEST_FQDIR/ | grep -P "\.(fastq|fq)$"`
  assertNotNull "$fqfiles"
}

## Check we find single index file (foo.00.c.tab)
testFindSubreadIndexRegexNotNull () {
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNotNull "$matches"
}

## It's invalid to find 2 or more index files
testMultipleSubreadIndexTypesReturnsNull () {
  touch "$S4M_TMP/$$/bar.00.b.tab"
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNull "$matches"
}

## It's invalid to find zero index files
testZeroSubreadIndexesReturnsNull () {
  rm -rf "$S4M_TMP/$$/"*.tab
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNull "$matches"
}

## Test find index by name
testIndexExistsByName () {
  idxbase="foo"
  indexdir="$S4M_TMP/$$"
  assertTrue "[ -f $indexdir/${idxbase}.00.c.tab ]"
}

## Base space is default if not specified
testBaseSpaceDefault () {
  baseorcs=""
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "base" "$baseorcs"
}

## Test we're picking up color space directive
testIsColorSpace () {
  baseorcs="color"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "color" "$baseorcs"
}

## Colour with the 'u' this time
testIsColourSpace () {
  baseorcs="colour"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "color" "$baseorcs"
}

## Base space is default if we get unknown value
testUnknownSpaceDefaultsToBase () {
  baseorcs="FOO"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "base" "$baseorcs"
}

## Test we have the R scripts we expect
testHaveRequiredRScripts () {
  assertTrue "[ -f ./scripts/subread_build_index.R ]"
  assertTrue "[ -f ./scripts/subread_fastq_to_bam.R ]"
  assertTrue "[ -f ./scripts/subread_feature_count_trans.R ]"
  assertTrue "[ -f ./scripts/subread_feature_count_gene.R ]"
  assertTrue "[ -f ./scripts/inc/util.R ]"
  assertTrue "[ -f ./scripts/inc/sequtil.R ]"
}

## Test subread index building - base space
## TODO: Complete!
##  - Use cut-down synthetic genome FASTA (as Subread does for testing)
testSubreadIndexBuildBaseSpace () {
  assertNotNull "TODO"
}

## Test subread index building - colour space
## TODO: Complete!
##  - Use cut-down synthetic genome FASTA (as Subread does for testing)
testSubreadIndexBuildBaseSpace () {
  assertNotNull "TODO"
}

## Test base-space alignment - single end
## TODO: Complete!
testBaseSpaceFASTQToBAMSingleEnd () {
  assertNotNull "TODO"
}

## Test base-space alignment - paired end
## TODO: Complete!
testBaseSpaceFASTQToBAMPairedEnd () {
  assertNotNull "TODO"
}

## Test colour-space alignment - single end
## TODO: Complete!
testColourSpaceFASTQToBAMSingleEnd () {
  assertNotNull "TODO"
}

## Test colour-space alignment - paired end
## TODO: Complete!
testColourSpaceFASTQToBAMPairedEnd () {
  assertNotNull "TODO"
}


